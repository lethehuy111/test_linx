<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Leave_request extends Model
{
    protected $table = 'leave_requests' ;
    protected $fillable = [
        'user_create_id',
        'user_approve_id',
        'reason',
        'time_start',
        'time_end',
        'status'
    ];
    public function userCreate(){
        return $this->belongsTo(User::class , 'user_create_id','id');
    }
    public function userApprove(){
        return $this->belongsTo(User::class , 'user_approve_id','id');
    }
}
