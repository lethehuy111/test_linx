<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Leave_request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LeaveRequestController extends Controller
{
    protected $user;
    protected $leave_request;

    public function __construct(User $user, Leave_request $leave_request)
    {
        $this->user = $user;
        $this->leave_request = $leave_request;
    }

    public function index()
    {
        $userId = Auth::user()->id;
        $leaveRequests = $this->leave_request->with('userApprove')->where('user_create_id', $userId)
            ->get()->toArray();
        foreach ($leaveRequests as $key => &$leaveRequest) {
            $leaveRequest['user_create'] = Auth::user()->name;
            $leaveRequest['user_approve'] = $leaveRequest['user_approve']['name'];
        }
        return view('leave_requests.home', compact('leaveRequests'));
    }

    public function create()
    {
        $users = $this->user->where('role', 1)->get();
        return view('leave_requests.create', compact('users'));
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $input['time_start'] = str_replace('T1', ' ', $input['time_start']);
        $input['time_end'] = str_replace('T1', ' ', $input['time_start']);
        $input['user_approve_id'] = Auth::user()->id;
        if ($input['reason'] == null) {
            $input['reason']  = " ";
        }
        $this->leave_request->create($input);

        return redirect('/');
    }

    public function getListLeaveRequest()
    {
        $userId = Auth::user()->id;
        $leaveRequests = $this->leave_request->with('userCreate')
            ->where('user_approve_id', $userId)
            ->where('status', 0)
            ->get()->toArray();
        foreach ($leaveRequests as $key => &$leaveRequest) {
            $leaveRequest['user_create'] = $leaveRequest['user_create']['name'];
        }
        return view('leave_requests.list-confirm', compact('leaveRequests'));
    }

    public function confirm($id)
    {
        $leaveRequest = $this->leave_request->where('id', $id)
            ->update(['status' => 1]);
        return redirect('/leave_request/confirm');
    }

    public function cancel($id)
    {
        $leaveRequest = $this->leave_request->where('id', $id)
            ->update(['status' => 2]);
        return redirect('/leave_request/confirm');
    }
}
