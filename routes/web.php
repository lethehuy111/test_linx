<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('leave_requests.home');
//})->middleware('auth');

Auth::routes();

Route::get('/', 'LeaveRequestController@index')->name('home')->middleware('auth');
Route::group(['middleware' => 'auth', 'prefix' => 'leave_request'],function (){
    Route::get('/' , 'LeaveRequestController@index');
    Route::get('/create' , 'LeaveRequestController@create');
    Route::post('/create' , 'LeaveRequestController@store');
    Route::get('/confirm' , 'LeaveRequestController@getListLeaveRequest');
    Route::get('/confirm/{id}' , 'LeaveRequestController@confirm');
    Route::get('/cancel/{id}' , 'LeaveRequestController@cancel');
});
