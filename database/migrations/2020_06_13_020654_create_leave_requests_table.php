<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_create_id');
            $table->unsignedBigInteger('user_approve_id');
            $table->text('reason');
            $table->string('time_start');
            $table->string('time_end');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();

            $table->foreign('user_create_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('user_approve_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_requests');
    }
}
