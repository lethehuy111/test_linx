<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'huylt',
            'email' => 'lethehuya6@gmail.com',
            'password' => Hash::make('1234'),
            'role' => 1,
        ] ,
        [
            'name' => 'hunglt',
            'email' => 'hunglt@gmail.com',
            'password' => Hash::make('1234'),
            'role' => 1,
        ]]);

    }
}
