@extends('layouts.app')
@
@section('content')
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Người xin phép</th>
            <th scope="col">Ngày bắt đầu nghỉ</th>
            <th scope="col">Ngày kết thúc</th>
            <th scope="col">Lý do</th>
            <th scope="col">Tên người phê duyêt </th>
            <th scope="col"> Trạng thái</th>
        </tr>
        </thead>
        <tbody>
            @foreach($leaveRequests as $leaveRequest)
                <tr>
                    <td>{{$leaveRequest['user_create']}}</td>
                    <td>{{$leaveRequest['time_start']}}</td>
                    <td>{{$leaveRequest['time_end']}}</td>
                    <td>{{$leaveRequest['reason']}}</td>
                    <td>{{$leaveRequest['user_approve']}}</td>
                    <td>
                        @switch($leaveRequest['status'])
                            @case(0)
                                <p style="color: #007bff">Chưa được xác nhận</p>
                            @break

                            @case(1)
                            Đã đồng ý
                            @break

                            @case(2)
                            Không đươc đồng ý
                            @break

                            @default
                            Không đươc đồng ý
                        @endswitch
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
