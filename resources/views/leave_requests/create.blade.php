@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Tạo đơn xin nghỉ :</h1>
        <form method="post" action="/leave_request/create">
            @csrf
            <div class="form-group">
                <select class="custom-select custom-select-lg mb-3" name="user_create_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{ $user->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="form-group">
                <label for="example-datetime-local-input" class="col-form-label">Ngày bắt đầu nghỉ :</label>
                <div>
                    <input class="form-control mb-3" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input" name="time_start">
                </div>
            </div>
            <div class="form-group">
                <label for="example-datetime-local-input" class="col-form-label">Nghỉ hết ngày :</label>
                <div>
                    <input class="form-control mb-3" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input" name="time_end">
                </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Lý do : </label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="reason" rows="4"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
