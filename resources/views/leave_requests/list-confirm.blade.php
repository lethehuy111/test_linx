@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Người xin phép</th>
                <th scope="col">Ngày bắt đầu nghỉ</th>
                <th scope="col">Ngày kết thúc</th>
                <th scope="col">Lý do</th>
                <th scope="col">Xác nhận</th>
            </tr>
            </thead>
            <tbody>
            @foreach($leaveRequests as $leaveRequest)
                <tr>
                    <td>{{$leaveRequest['user_create']}}</td>
                    <td>{{$leaveRequest['time_start']}}</td>
                    <td>{{$leaveRequest['time_end']}}</td>
                    <td>{{$leaveRequest['reason']}}</td>
                    <td>
                        <a href="/leave_request/confirm/{{$leaveRequest['id']}}" class="btn btn-primary mr-2">Xác nhận</a>
                        <a href="/leave_request/cancel/{{$leaveRequest['id']}}" class="btn btn-danger">Hủy</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

